# Computer Music: Physical Modeling
## Lesson 1 - Introduction to Physical Modeling

### Overview
- **Introduction to the basics of physical modeling in computer music.**
- **Focus on simulating the motion of point-like bodies under forces such as gravity and collisions.**
- **Exploration of how these models can be used to generate control signals in music production.**

#### Intuitive Insight
- _"Physical modeling synthesizes sound by simulating the physical source of sound... as opposed to simulating the sound itself."_ - Julius O. Smith
- Imagine directing a play where characters are sounds, and their movements and interactions create the music.

### Newton's Laws and Digitizing Motion
- **Understanding Newton's Laws:** Mass, force, position, velocity, and acceleration.
- **Digitizing Newton's Laws for modeling:**
  - **Gravity** in multi-body simulations.
  - **Collisions** between moving bodies.
  - **Control signals** generation for music.

#### Intuitive Insight
- Newton's laws are like rules of a game. Digitizing them is like programming a computer to play this game, observing how objects behave under these rules.

### Basics of Calculus in Physical Modeling
- **Understanding Integrals and Derivatives:** Integral of velocity equals position; derivative represents the rate of change of velocity.
- **Approximating derivatives and integrals** using differences and sums.

#### Intuitive Insight
- Derivatives tell us "how fast" something is changing, like a snapshot of speed at any instant. Integrals add up these changes to give a complete picture, like tracking the total journey of a car over time.

### Harmonic Oscillator - Mass-Spring-Damper Systems
- **Understanding a mass-spring system:** Connecting mass to a spring and observing motion.
- **Differential Equations:** Introduction in the mass-spring oscillator context.
- **Finite Difference Schemes:** Computational methods for simulating oscillator behavior.
- **Coupled Oscillators:** Exploring interconnected oscillator systems.
- **Importance of the Harmonic Oscillator** in modern physical modeling.

#### Intuitive Insight
- _"The simplest mechanical resonating system is a mass attached to a linear spring."_ - Julius O. Smith
- Picture a child on a trampoline: their movements and how the trampoline reacts are similar to a mass-spring system's dynamics.

### Nonlinear Oscillators and Chaos
- **Exploring Van der Pol and Duffing oscillators.**
- **Understanding non-linear dynamics and chaotic behavior.**

#### Intuitive Insight
- Nonlinear oscillators are like weather systems: predictable up to a point, then becoming complex and chaotic, yet following underlying patterns.

### Mass Interaction Models and Lumped Models
- **From 'cordis anima' to MI-Gen Library:** Evolution of mass interaction models.
- **Modeling system behaviors** as approximations, providing unique perspectives.

#### Intuitive Insight
- Think of mass interaction models as a dance where each 'dancer' (atom or mass) moves according to the 'music' (forces) around them.

### Vibrating Systems and Wave Phenomena
- **Fundamentals:** Wavelength, frequency, and velocity in vibrating systems.
- **Understanding traveling and standing waves** in strings, pipes, and membranes.
- **Modes of Vibration:** Exploring concepts with examples.

#### Intuitive Insight
- Vibrations in a string are akin to ripples in a pond. How these ripples behave (standing still or moving) reveals much about the forces and materials involved.

### Digital Waveguides and Karplus-Strong Algorithm
- **Digital Waveguides:** Using delay lines to simulate string vibrations.
- **Karplus-Strong Algorithm:** Modeling strings with feedback comb filters and tuned delay lines.

#### Intuitive Insight
- Digital waveguides act like echo chambers for sound, creating a simulation of how real strings would vibrate and sound.

### Superposition and Modal Resonators
- **Understanding Harmonics:** Superposition of standing waves.
- **Modal Synthesis:** Analyzing and resynthesizing vibration modes.
- **Modeling Impulsive Forces:** Effects on vibrational modes.

#### Intuitive Insight
- Imagine each harmonic as an instrument in an orchestra, playing together to create a rich, complex tone.

### Advanced Topics in Modal Synthesis
- **The String Equation:** Understanding partial derivatives, stiffness, damping, dispersion, and nonlinear tensions.
- **Membranes and Plucking Models:** Different methods of exciting strings and membranes.

#### Intuitive Insight
- Understanding the string equation is like figuring out the recipe for a cake. Each ingredient changes the final product, just as the properties of a string affect its sound.

### Interactive Physical Modeling
- **Energy Transmission:** From external agents to air cavities.
- **Nonlinear Interactions:** Integrating interactions with modal resonators.

#### Intuitive Insight
- Imagine a game of pool. The balls' interactions and energy transfers simulate how energy is transmitted and filtered in physical modeling.

### Modeling Interactions and Collisions
- **Different Types of Interactions:** Hitting, plucking, scratching, bumping.
- **Collisions and Barriers:** Simulating complex interactions between bodies.

#### Intuitive Insight
- Modeling these interactions is like creating a virtual playground. Each action leads to unique sounds and musical expressions.

### Real-time Bowed String Model
- **Practical Implementation:** Using tools like Max MSP and Gen for real-time modeling.

---

## Prerequisites for Understanding Physical Modeling
- Basic mathematical functions, trigonometry, real numbers.
- Understanding position and velocity as functions of time.
- Newton's second law (F = ma), sinusoidal functions, logarithms.
- Energy concepts (kinetic, potential), basic calculus (derivatives, integrals).
- Programming concepts in environments like Max MSP/Gen, Pure Data, Jupyter Notebook.

---

## Assignments
- **Due Date:** June 18, 2024
- **Task:** Create a comprehensive report on physical modeling experiments.
- **Implementations:** Python, C++, SuperCollider, Max MSP/Gen.
- **Focus:** Audio-visual rendering for physical modeling.

---
